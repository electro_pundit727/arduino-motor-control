This project is control the state of motors of 3 using ArduinoUno.

Each motor works as 9 options by pressing of 1~9 key on the keypad.

Each motor also works as user option mode by pressing of 0 key.

If '*' key is pressed, all of motors are stopped. 

And it is impossible to switch the mode.

If '*' key is pressed again, it is possible to switch the mode. 

Prototype is the following. 
The potentiometer is to control the contrast of LCD. It is 10Kohms

![alt text](prototype.jpg)

Below are a table and pictures showing how to connect Arduino and Keypad.

![alt text](keypad.jpg)

![alt text](JHD162A.png)


| Arduino Uno  | 4*4 keyPad |Arduino Uno | JHD162A    | Arduino    |LED         |       
| :-----------:|:----------:|:----------:|:----------:|:----------:|:----------:|
| Digital1     | 1          |+5V         | 2, 15      |Analog2     |Blue        |    
| Digital2     | 2          |GND         | 1, 5, 16   |Analog1     |Yellow      |
| Digital3     | 3          |Digital12   |4           |Analog3     |Green       |
| Digital4     | 4          |GND         |6           |Analog0     |Red(Alarm)  |
| Digital5     | 5          |Digital8    |11          |            |            |
| Digital6     | 6          |Digital9    |12          |            |            |       
| Digital7     | 7          |Digital10   |13          |            |            |
| Analog 5     | 8          |Digital11   |14          |            |            |

Install Library

  We have to use for this project.
  1. Keypad.h
  2. [MsTimer2.zip](https://bitbucket.org/electro_pundit727/arduino-motor-control/src/a71ce4287291?at=master)
  3. LiquidCrystal.h
  



