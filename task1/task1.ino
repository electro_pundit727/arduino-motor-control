#include <Keypad.h>
#include <LiquidCrystal.h>
#include <MsTimer2.h>

/***************** setting for the keypad  ******************/

const byte numRows = 4;    // number of rows on the keypad
const byte numCols = 4;    // number of columns on the keypad

// keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols] =
{
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

// Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {A5, 7, 6, 5};    // Rows 0 to 3
byte colPins[numCols] = {4, 3, 2, 1};     // Columns 0 to 3

// Initializes an instance of the Keypad class
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

/************** setting for the LCD JHD162A  *****************/

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 13, 8, 9, 10, 11);

/************** setting for the motor relay  *****************/

int motor1_Pin = A1;  // yellow led
int motor2_Pin = A2;  // blue led
int motor3_Pin = A3;  // green led

int alarm_led = A0;  // Turn on the red led when a errors are occured.

char keypressed;
int keyMode;
boolean stopKey_state;
byte key4_state, key6_state, key7_state, key8_state, key9_state;

void setup() {

  pinMode(motor1_Pin, OUTPUT);
  pinMode(motor2_Pin, OUTPUT);
  pinMode(motor3_Pin, OUTPUT);
  pinMode(alarm_led, OUTPUT);

  stopKey_state = 0;  // this variable is 1 when '*'key is pressed once.

  // setup timer interrupt
  MsTimer2::set(50, GetKey);  //  get the key value by 50ms
  MsTimer2::start();

  Serial.begin(9600);
  lcd.begin(16, 2);   // initializes the 16x2 LCD
  lcd.print("Stand by...");
}

void MotorMode1() {       // Mode 1

  Serial.println("Selected the mode 1.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 1");

  int original_keyMode = keyMode;   // save the original keyMode

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (keyMode == original_keyMode) {      // check if current keyMode value same to original.
    digitalWrite(motor3_Pin, HIGH);
    Motor_Delay(5000, original_keyMode);
    digitalWrite(motor3_Pin, LOW);
    Motor_Delay(500, original_keyMode);
  }

  if (keyMode == original_keyMode) {
    digitalWrite(motor1_Pin, HIGH);
    Motor_Delay(2000, original_keyMode);
  }

  digitalWrite(motor1_Pin, LOW);
  Motor_Delay(500, original_keyMode);
  if (keyMode == original_keyMode) {
    digitalWrite(motor2_Pin, HIGH);
    Motor_Delay(3000, original_keyMode);
  }
}
void MotorMode2() {     //Mode 2

  Serial.println("Selected the mode 2.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 2");

  int original_keyMode = keyMode;

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (keyMode == original_keyMode) {
    digitalWrite(motor2_Pin, HIGH);
    Motor_Delay(4000, original_keyMode);
  }

  digitalWrite(motor2_Pin, LOW);
  Motor_Delay(1000, original_keyMode);

  if (keyMode == original_keyMode) {
    digitalWrite(motor3_Pin, HIGH);
    Motor_Delay(3000, original_keyMode);
  }

  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(2000, original_keyMode);
}

void MotorMode3() {    // Mode3

  Serial.println("Selected the mode 3.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 3");

  delay(500);
}

void MotorMode4() {   // Mode 4

  Serial.println("Selected the mode 4.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 4");

  int original_keyMode = keyMode;

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (!key4_state) {
    digitalWrite(motor2_Pin, HIGH);
    Motor_Delay(5000, original_keyMode);    
  }
  else {
    digitalWrite(motor2_Pin, HIGH);
    Motor_Delay(10000, original_keyMode);
  }
}

void MotorMode5() {   // Mode5

  Serial.println("Selected the mode 5.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 5");

  delay(500);
}

void MotorMode6() {  // Mode6

  Serial.println("Selected the mode 6.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 6");

  int original_keyMode = keyMode;

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (!key6_state) {
    digitalWrite(motor3_Pin, HIGH);
    Motor_Delay(5000, original_keyMode);    
  }
  else {
    digitalWrite(motor3_Pin, HIGH);
    Motor_Delay(11000, original_keyMode);
  }
}

void MotorMode7() {    //Mode7

  Serial.println("Selected the mode 7.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 7");

  int original_keyMode = keyMode;

  digitalWrite(motor1_Pin, LOW);
  //digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (!key7_state) {
    digitalWrite(motor2_Pin, HIGH);
  }
  else {
    digitalWrite(motor2_Pin, LOW);
  }
}

void MotorMode8() {    //Mode8

  Serial.println("Selected the mode 8.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 8");

  int original_keyMode = keyMode;

  //digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (!key8_state) {
    digitalWrite(motor1_Pin, HIGH);
  }
  else {
    digitalWrite(motor1_Pin, LOW);
  }
}

void MotorMode9() {  //Mode 9

  Serial.println("Selected the mode 9.");
  lcd.setCursor(0, 0);
  lcd.print("Now is working...");
  lcd.setCursor(0, 1);
  lcd.print("Mode 9");

  int original_keyMode = keyMode;

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  //digitalWrite(motor3_Pin, LOW);
  Motor_Delay(500, original_keyMode);

  if (!key9_state) {
    digitalWrite(motor3_Pin, HIGH);
  }
  else {
    digitalWrite(motor3_Pin, LOW);
  }
}

void AlarmMode() {     // When * key is pressed, come here.

  if (stopKey_state == 1) {       // check if * key is pressed to stop the motors.

    Serial.println("Occure error.");
    lcd.setCursor(0, 0);
    lcd.print("Occure error...");
    lcd.setCursor(0, 1);
    lcd.print("Press*");

    digitalWrite(alarm_led, HIGH);
  }

  digitalWrite(motor1_Pin, LOW);
  digitalWrite(motor2_Pin, LOW);
  digitalWrite(motor3_Pin, LOW);

  key4_state = 0;        // format previous state of 4, 6~9
  key6_state = 0;
  key7_state = 0;
  key8_state = 0;
  key9_state = 0;
}

void Motor_Delay(int iDelayTime, int ori_mode) {

  for (int i = 0; i < iDelayTime; i += 10) {
    if (keyMode != ori_mode) break;
    delay(10);
  }
}

void GetKey() {                   // timer interrupt function. This function is called by 50ms.

  keypressed = myKeypad.getKey();
  if (keypressed != NO_KEY) {        //check if anykey is pressed.
    switch (keypressed)
    {
      case '1': keyMode = 1; break;
      case '2': keyMode = 2; break;
      case '3': keyMode = 3; break;
      case '4': {
          keyMode = 4;
          key4_state = !key4_state;
        } break;
      case '5': keyMode = 5; break;
      case '6': {
          keyMode = 6;
          key6_state = !key6_state;
        } break;
      case '7': {
          keyMode = 7;
          key7_state = !key7_state;
        } break;
      case '8': {
          keyMode = 8;
          key8_state = !key8_state;
        } break;
      case '9': {
          keyMode = 9;
          key9_state = !key9_state;
        }; break;
      case '*': {
          keyMode = 0;
          stopKey_state = !stopKey_state;
        } break;
      default:
        Serial.println("Nothing is pressed key.");
    }
  }
}
void loop() {

  if (stopKey_state) {        // check if * key is pressed

    AlarmMode();
  }

  if (!stopKey_state) {

    digitalWrite(alarm_led, LOW);
    switch (keyMode)
    {
      case 1: MotorMode1(); break;
      case 2: MotorMode2(); break;
      case 3: MotorMode3(); break;
      case 4: MotorMode4(); break;
      case 5: MotorMode5(); break;
      case 6: MotorMode6(); break;
      case 7: MotorMode7(); break;
      case 8: MotorMode8(); break;
      case 9: MotorMode9(); break;
      case 0: AlarmMode();  break;
      default:
        Serial.println("Invalid entry");
    }
  }
}




