
/************** example for using the Timer.h library ***************/

#include <Timer.h>

Timer t;
int timeID;
int count = 10;

void setup() {
  Serial.begin(9600);
  timeID = t.every(1000, countdown);
}

void countdown() {
  count -= 1;
  //Serial.print(timeID);
  //Serial.print("     ");
  Serial.println(count);
}
void loop() {

  t.update();
  if (count == 0) t.stop(timeID);

}

/************** example for using the Timer.h library ***************

#include <SimpleTimer.h>

SimpleTimer timer;
int timeID;
int count = 8000;

void setup() {
  Serial.begin(9600);
  timeID = timer.setTimeout(count, StopMotor);
}

void StopMotor() {
  Serial.println("Motor is stopped.");
}

void loop() {


  while (1) {
    timer.run();
    delay(1000);
    timer.disable(timeID);
    //Serial.print("stop");
    delay(10000);
    Serial.print("stop");
    timer.toggle(timeID);
  }
}*/

